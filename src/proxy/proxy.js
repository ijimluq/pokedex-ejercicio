var express = require('express');
var morgan = require('morgan');
var proxy = require('http-proxy-middleware');
var ports = require('../../ecosystem.ports.js');

var port = process.env.PORT || 8080;
var app = express();
app.use(morgan('dev'));

const apiOption = {
  target: `http://localhost:${ports.api}`,
  changeOrigin: true,
  pathRewrite: { '^/api/': '/' }
};
const apiProxy = proxy.createProxyMiddleware('/api' , apiOption);
app.use(apiProxy);

const appOption = {
  target: `http://localhost:${ports.app}`,
  changeOrigin: true
};
const appProxy = proxy.createProxyMiddleware('/' , appOption);
app.use(appProxy);

app.listen(port, function() {
  console.log('listening to port', port);
});
