import { Component, Input } from '@angular/core';
import { IPokemonDetails } from 'src/app/shared/pokemon.model';

@Component({
  selector: 'app-pokemon-card-simple',
  templateUrl: './pokemon-card-simple.component.html',
  styleUrls: ['./pokemon-card-simple.component.scss']
})
export class PokemonCardSimpleComponent {
  @Input()
  pokemon: IPokemonDetails;
}
