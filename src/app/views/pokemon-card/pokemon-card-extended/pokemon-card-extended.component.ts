import { Component, Input, OnInit } from '@angular/core';
import { PokedexService } from 'src/app/shared/pokedex.service';
import { IPokemonDetails } from 'src/app/shared/pokemon.model';

@Component({
  selector: 'app-pokemon-card-extended',
  templateUrl: './pokemon-card-extended.component.html',
  styleUrls: ['./pokemon-card-extended.component.scss']
})
export class PokemonCardExtendedComponent implements OnInit {
  @Input()
  pokemon: IPokemonDetails;

  pokemonDescription: string;

  constructor(private pokedexService: PokedexService) {}

  ngOnInit(): void {
    this.getPokemonDescription();
  }

  private getPokemonDescription(): void {
    this.pokedexService
      .getPokemonDescriptionById(this.pokemon.id)
      .subscribe(
        (flavor_text: string) => (this.pokemonDescription = flavor_text)
      );
  }
}
