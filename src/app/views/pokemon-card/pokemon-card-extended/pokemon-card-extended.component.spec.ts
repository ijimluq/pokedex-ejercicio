import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonCardExtendedComponent } from './pokemon-card-extended.component';

describe('PokemonCardExtendedComponent', () => {
  let component: PokemonCardExtendedComponent;
  let fixture: ComponentFixture<PokemonCardExtendedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonCardExtendedComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PokemonCardExtendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
