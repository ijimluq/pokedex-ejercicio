import { Component, Input } from '@angular/core';
import { PokedexService } from 'src/app/shared/pokedex.service';
import { IPokemonDetails } from 'src/app/shared/pokemon.model';
import { ECardTypes } from './models/cardType';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent {
  @Input()
  pokemon: IPokemonDetails;

  @Input()
  pokemonSelected: number;

  cardTypes = ECardTypes;
  constructor(private pokedexService: PokedexService) {}

  getPokemonCardColor(): string {
    return this.pokedexService.getPokemonCardColor(this.pokemon);
  }
}
