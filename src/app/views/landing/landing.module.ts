import { NgModule } from '@angular/core';
import { LandingComponent } from './landing.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PokemonCardExtendedComponent } from '../pokemon-card/pokemon-card-extended/pokemon-card-extended.component';
import { PokemonCardComponent } from '../pokemon-card/pokemon-card.component';
import { PokemonCardSimpleComponent } from '../pokemon-card/pokemon-card-simple/pokemon-card-simple.component';

@NgModule({
  imports: [SharedModule],
  declarations: [
    LandingComponent,
    PokemonCardExtendedComponent,
    PokemonCardComponent,
    PokemonCardSimpleComponent
  ],
  exports: [LandingComponent]
})
export class LandingModule {}
