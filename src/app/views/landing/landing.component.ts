import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs';
import { EActions } from 'src/app/core/models/landing.component.model';
import { PokedexService } from 'src/app/shared/pokedex.service';
import {
  IPokemonDetails,
  IPokemonListApiResponse,
  IPokemonListItemApiResponse
} from 'src/app/shared/pokemon.model';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  constructor(private pokedexService: PokedexService) {}

  pokemonList: IPokemonListItemApiResponse[] = [];
  pokemonDetailList: IPokemonDetails[] = [];
  pokemonSelected: number = 0;
  nextoffSet: number;

  ngOnInit(): void {
    this.getPokemonList(20, 0, EActions.Add);
  }

  // Esta parte podriamos hacerla en el servicio del componente y ademas ordenarlo cuando nos llegue
  // En lugar de por cada pokemon
  getPokemonList(limit: number, offset: number, action: EActions): void {
    this.pokedexService
      .getPokemonList(limit, offset)
      .subscribe((pokemonListResponse: IPokemonListApiResponse) => {
        this.nextoffSet = offset + limit;
        if (action === EActions.Add) {
          this.pokemonList = [
            ...this.pokemonList,
            ...pokemonListResponse.results
          ];
          this.getPokemonDetailList(pokemonListResponse);
        } else if (action === EActions.Replace) {
          // esta parte esta pensada para paginar en lugar de cargar más aunque luego realmente no se use
          this.pokemonList = [...pokemonListResponse.results];
          this.pokemonDetailList = [];
          this.getPokemonDetailList(pokemonListResponse);
        }
      });
  }

  onClick(pokemon: IPokemonDetails): void {
    if (pokemon.id === this.pokemonSelected) {
      this.pokemonSelected = 0;
    } else {
      this.pokemonSelected = pokemon.id;
    }
  }

  private getPokemonDetailList(
    pokemonListResponse: IPokemonListApiResponse
  ): void {
    for (const pokemon of pokemonListResponse.results) {
      this.getPokemonDetail(pokemon.url);
    }
  }

  private getPokemonDetail(url: string): void {
    this.pokedexService
      .getPokemonDetailsWithURL(url)
      .pipe(
        finalize(() => {
          if (this.pokemonDetailList.length === this.pokemonList.length)
            this.pokemonDetailList.sort(
              (pokemonA: IPokemonDetails, pokemonB: IPokemonDetails) =>
                pokemonA.id - pokemonB.id
            );
        })
      )
      .subscribe(
        (pokemonDetail: IPokemonDetails) =>
          (this.pokemonDetailList = [...this.pokemonDetailList, pokemonDetail])
      );
  }
}
