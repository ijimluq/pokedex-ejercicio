import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { ViewsModule } from './views/views.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    CoreModule,
    LayoutModule, // <app-layout>
    ViewsModule // routes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
