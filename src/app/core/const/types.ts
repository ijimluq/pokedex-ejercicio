import { IPokemonType } from '../models/landing.component.model';

export const pokemonTypes: IPokemonType[] = [
  {
    name: 'normal',
    color: '#9f9f9f'
  },
  {
    name: 'fighting',
    color: '#c82525'
  },
  {
    name: 'flying',
    color: '#75dfdc'
  },
  {
    name: 'poison',
    color: '#983dc6'
  },
  {
    name: 'ground',
    color: '#8c6d1a'
  },
  {
    name: 'rock',
    color: '#d9a92a'
  },
  {
    name: 'bug',
    color: '#26ef0c'
  },
  {
    name: 'ghost',
    color: '#740497'
  },
  {
    name: 'steel',
    color: '#848284'
  },
  {
    name: 'fire',
    color: '#d03535'
  },
  {
    name: 'water',
    color: '#3881f1'
  },
  {
    name: 'grass',
    color: '#38f14b'
  },
  {
    name: 'electric',
    color: '#e1dd55'
  },
  {
    name: 'psychic',
    color: '#e1559f'
  },
  {
    name: 'ice',
    color: '#75e5cb'
  },
  {
    name: 'dragon',
    color: '#18309d'
  },
  {
    name: 'dark',
    color: '#050d32'
  },
  {
    name: 'fairy',
    color: '#b109cc'
  }
];
