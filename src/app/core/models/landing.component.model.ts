export enum EActions {
  Add,
  Replace
}

export interface IPokemonType {
  name: string;
  color: string;
}
